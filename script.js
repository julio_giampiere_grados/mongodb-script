//Nombre de la base de Datos
var db = db.getSiblingDB('mongoTutorial');

//Eliminar Base de Datos
db.dropDatabase();

//Usuarios
var firstNames = ['Julio', 'Edwin', 'Juan', 'Paul', 'Marlith', 'Wos'];
var lastNames = ['Grados', 'Gonzales', 'Pino', 'Alayo', 'Quiroz'];
var UsersList = [];


for (var i = 0; i < firstNames.length; i++) {
    let single = null;
    if ( i!= 2){
        single = true;
    }else {
        single = false;
    }
    var user = {
        name: {
            first: firstNames[i],
            last: lastNames[i]
        },
        sigle: single
    };
    UsersList.push(user);
}

//insertar usuarios
db.users.insert(UsersList);


var authors = db.users.find().toArray();

var titles = ['Insert', 'Find', 'Remove', 'Update'];
var description = "Crud de MongoDB"
var body = "Las 4 funciones principales de una base de datos"

var postsRaw = [];

//creamos los post

for(var i = 0; i < titles.length; i++) {
    var post = {
        title: titles[i],
        description: description,
        body: body,
        author: authors[Math.floor(Math.random() * authors.length)]._id
    }
    postsRaw.push(post);
}

//insertamos los post 
db.posts.insert(postsRaw);
